data Expr = C Float | Expr :+ Expr | Expr :- Expr
          | Expr :* Expr | Expr :/ Expr
          | V String
          | Let String Expr Expr
     deriving Show

-- New HOF for Expr
-- newHOF :: (Float -> Float) -> Expr -> Float
-- newHOF _ (C x) = x
-- newHOF f (C x) = f x

-- evaluate function
evaluate (C x) = x
evaluate (e1 :+ e2) = evaluate e1 + evaluate e2
evaluate (e1 :- e2) = evaluate e1 - evaluate e2
evaluate (e1 :* e2) = evaluate e1 * evaluate e2
evaluate (e1 :/ e2) = evaluate e1 / evaluate e2
evaluate (Let v e0 e1) = evaluate (subst v e0 e1)
evaluate (V _) = evaluate (C 0) -- karena begitulah

subst v0 e0 (V v1) = if (v0 == v1) then e0 else (V v1)
subst _ _ (C c) = (C c)
subst v0 e0 (e1 :+ e2) = subst v0 e0 e1 :+ subst v0 e0 e2
subst v0 e0 (e1 :- e2) = subst v0 e0 e1 :- subst v0 e0 e2
subst v0 e0 (e1 :* e2) = subst v0 e0 e1 :* subst v0 e0 e2
subst v0 e0 (e1 :/ e2) = subst v0 e0 e1 :/ subst v0 e0 e2
subst v0 e0 (Let v1 e1 e2) = Let v1 e1 (subst v0 e0 e2) -- perlu koreksi ???
-- subst v0 e0 (Let v1 e1 e2) = subst ((v1, e1): v0 e0) e2 --error

-- evaluate with new HOF
-- newHOF :: (Float -> Float) -> Expr -> Float
-- newHOF _ (C x) = x
-- newHOF f (C x) = f x

-- attempt to use Expr
-- newHOF ((C x) Expr (C y)) = x Expr y [Failed]

-- newHOF Expr :+ Expr = Expr + Expr [Failed]
-- succeed mimicing evaluate but is it higher order function?
-- newHOF ((C x) :+ (C y)) = sum x y [can't use sum here]
-- newHOF ((C x) :+ (C y)) = sum [x, y]
-- newHOF :: Expr -> (Float -> Float -> t) -> Expr -> t
-- newHOF (C x) f (C y) = x `f` y
-- newHOF ((C x) :+ (C y)) = x + y
-- newHOF ((C x) :- (C y)) = x - y
-- newHOF ((C x) :* (C y)) = x * y
-- newHOF ((C x) :/ (C y)) = x / y


-- How to use :+ and :- instead of `f` ??
-- newHOF (C x) :+ (C y) = newHOF (C x) (+) (C y)

-- newHOF with fold
-- newHOF ((C x) :+ (C y)) = foldl (+) 0 (C x) (C y)
-- is this hof though
-- newHOF :: Expr -> Float

-- newHOF ((C x) :+ (C y)) = plus
--                         where
--                             plus = foldl (+) 0 [x,y]
-- newHOF (e1 :- e2) = foldl (-) 0 [x, y]
-- newHOF ((C x) :* (C y)) = foldl (*) 0 [x, y]
-- newHOF ((C x) :/ (C y)) = foldl (/) 0 [x, y]




-- For the subst fix
evaluate' env (Let v1 e1 e2) = evaluate' ((v1, e1):env) e2
-- where env = v0 e0 in the question, and evaluate' = subst
-- hence, subst v0 e0 (Let v1 e1 e2) = subst ((v1, e1): v0 e0) e2


-- For the evaluate (V _) fix, the value should be the _ string however,
-- how should we know? use id


mapExp f (C x) = C (f x)
mapExp f (V _) = C (f 0.0)
mapExp f (e1 :+ e2) = mapExp f e1 :+ mapExp f e2
mapExp f (e1 :* e2) = mapExp f e1 :* mapExp f e2
mapExp f (e1 :- e2) = mapExp f e1 :- mapExp f e2
mapExp f (e1 :/ e2) = mapExp f e1 :/ mapExp f e2


-- Expression Fold 
-- foldExp (add, mult, subt, div) (C x) = x
-- foldExp (add, mult, subt, div) (x:+y) = add (foldExp (add, mult, subt, div) f x) (foldExp (add, mult, subt, div) f y)
-- foldExp (add, mult, subt, div) f (x:*y) = mult (foldExp (add, mult, subt, div) f x) (foldExp (add, mult, subt, div) f y)
-- foldExp (add, mult, subt, div) f (x:-y) = subt (foldExp (add, mult, subt, div) f x) (foldExp (add, mult, subt, div) f y)
-- foldExp (add, mult, subt, div) f (x:/y) = div (foldExp (add, mult, subt, div) f x) (foldExp (add, mult, subt, div) f y)

-- newEval = foldExp (add, mult, subt, div)
--           where add = (+)
--                 mult = (*)
--                 subt = (-)
--                 div = (/)

