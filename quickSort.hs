
-- Fail no list comprehension on both quicksort recursive calls
-- quickSort (x:xs) = quickSort(y | y <-xs, y <= x ) (++) [x] (++) quickSort(y | y <- xs, y > x)

-- Somehow this doesn't work...
-- quickSort []     = []
-- quickSort [x]    = [x]
-- quickSort (x:xs) = quickSort [y | y <-xs, y <= x ] (++) [x] (++) quickSort [y | y <- xs, y > x]


-- Apparently if we add (++) in the quickSort recursive calls it would return error 
-- because of concatenation things, instead of (++) we should remove the brackets and
-- use ++ plainly
quickSort []      = []
quickSort [x]     = [x]
quickSort (x:xs)  = quickSort [y | y <- xs, y<=x] ++ [x] ++ quickSort [y | y <- xs, y>x]
