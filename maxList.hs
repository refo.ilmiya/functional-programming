maxList [] = 0
maxList (x:xs) = max x (maxList xs)
maxListFold xs = foldl max 0 xs
