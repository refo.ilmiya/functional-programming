-- merge [] kanan = kanan
-- merge kiri [] = kiri
-- merge kiri@(x:xs) kanan@(y:ys)  | x < y = x: merge xs kanan
--                                 | y <= x = y: merge kiri ys

-- merge [] kanan         = kanan
-- merge kiri []         = kiri
-- merge (x:xs) (y:ys) | x < y     = x:merge xs (y:ys)
--                     | otherwise = y:merge (x:xs) ys

-- mergeSort xs = merge (mergeSort kiri) (mergeSort kanan)
--    where kiri = take ((length xs) `div` 2) xs
--          kanan = drop ((length xs) `div` 2) xs

merge [] ys         = ys
merge xs []         = xs
merge (x:xs) (y:ys) | x < y     = x:merge xs (y:ys)
                    | otherwise = y:merge (x:xs) ys
halve xs = (take lhx xs, drop lhx xs)
           where lhx = length xs `div` 2
msort []  = []
msort [x] = [x]
msort  xs = merge (msort left) (msort right)
            where (left,right) = halve xs