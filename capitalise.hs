import qualified Data.Char as Char

getWord word [] = (word,[])
getWord word (x:xs) | x == ' ' = (word, xs)
                    | x /= ' ' = getWord (word ++ [x]) xs  

splitter lw []  = lw
splitter lw inp = let (word, rest) = getWord "" inp
                  in splitter (lw ++ [word]) rest 

upper el word = if word `elem` el 
                then word 
                else Char.toUpper (head word) : (tail word)

combine []  = []
combine [a] = a
combine (a:xs) = a ++ " " ++ (combine xs)

capitalise el  =  combine . (map (upper el)) . (splitter []) 