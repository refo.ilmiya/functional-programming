import Data.List
-- it would return error at character \\ if we didnt' import the data.list
-- module
-- base case should use [[]] because we concate lists in x:ps
perm [] = [[]]
perm ls = [x:ps | x <- ls, ps <- perm(ls \\ [x]) ]

-- attempt to use foldl [failed]
-- permfold ls = foldl  [[]] ls