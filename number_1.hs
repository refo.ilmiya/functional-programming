{-(*) Define the length function using map and sum.-}
length' :: [a] -> Int
length' xs = sum (map (\x -> 1) xs)
{- explanation: 
(map (\x -> 1) xs) will replace each element of xs into 1 and returns its array of all 1s
sum of that array would be equal to its initial array's size
-}

{-
*) What does map (+1) (map (+1) xs) do? Can you conclude anything in
general about properties of map f (map g xs), where f and g are arbitrary
functions?
-}

map_plus xs = map (+1) (map (+1) xs) 
{-
means, the function would do +2 to each element of xs

in general, we can conclude that the properties of map f (map g xs) is f(g(xs))
-} 

{-
	• Give the type of, and define the function iter so that
	• iter n f x = f (f (... (f x)))
	where f occurs n times on the right-hand side of the equation. For instance, we
	should have
	iter 3 f x = f (f (f x))
	and iter 0 f x should return x.
-}

iter :: Int -> (a -> a) -> (a -> a)
iter 0 f x = x
iter n f x = f (iter (n-1) f x)

-- iter :: Int -> (a -> a) -> (a -> a) means it takes three arguments, an integer and
-- a function with an arbitary function which returns anything based on its funfction
-- iter 0 f x = x is the base case
-- iter n f x = f (iter (n-1) f x) is the recursive case

{-
	What is the type and effect of the following function?
	• \n -> iter n succ
	succ is the successor function, which increases a value by one:
	Prelude> succ 33
	34
-}

-- prop_IterN x y =
--   x >= 0 && y >= 0 ==>
--     f1 x y == f2 x y
--  where
--   f1 = (\n -> iter n succ) :: Int -> Int -> Int
--   f2 = (+)

-- If x more than 0 and y more than 0, iter x succ y will be equal to x + y
-- the type of (\n -> iter n succ) is a lambda function that will take a variable and 
-- put it as iterator for succ function for an arbitary number 

{-
	(*) How would you define the sum of the squares of the natural numbers 1
	to n using map and foldr?
	• How does the function
	• mystery xs = foldr (++) [] (map sing xs)
	• where
	• sing x = [x]
	behave?
-}
------------------------------------------------------------------------------------
------------------------------------ Bookmark --------------------------------------
sumSquares :: Integer -> Integer
sumSquares n = foldr (+) 0 $ map (\x -> x*x) [1..n]
{-
sumSquares function would first create a list conists of numbers from 1 until n.
map (\x -> x*x) [1..n] will create a list of squaredNumbers from 1 through n.
Then, for each of that element sumSquares would calculate the sum of all its elements
-}

mystery xs = foldr (++) [] (map sing xs)
 where
  sing x = [x]
{-
mystery function will add any input xs into the empty list [], however the order will be
from the input list first. For example, if the initial list in mystery function is [1]
and the input is [2,3] then after the calling of mystery [2,3] the result would be 
[2,3,1]
-}


prop_Mystery xs = mystery xs == (xs :: [Int])


{-
	(*) If id is the polymorphic identity function, defined by id x = x, explain the
	behavior of the expressions
	• (id . f) (f . id) (id f)
	If f is of type Int -> Bool, at what instance of its most general type a ->
	a is id used in each case?
-}

{-
	(id . f)  is the same as f, because the result of f is fed to the
	          identity function id, and thus not changed
	(id :: Bool -> Bool)

	(f . id)  is the same as f, because the argument of f is fed to the
	          identity function id, and thus not changed
	(id :: Int -> Int)

	id f      is the same as f, because applying id to something does not
	          change it
	(id :: (Int -> Bool) -> (Int -> Bool))
-}

{-
	Define a function composeList which composes a list of functions into a single
	function. You should give the type of composeList, and explain why the function
	has this type. What is the effect of your function on an empty list of functions?
-}

composeList :: [a -> a] -> (a -> a)
composeList []     = id
composeList (f:fs) = f . composeList fs

{-
The compose list would have the type [a -> a] -> (a -> a) because we want composeList
to return a function, and a funciton should have input a and an output a.
If the list is empty, we would return id, a default expression of itself.
If there is an element inside the list, recursively we compose those elements with '.'.
A composition function expression in haskell.
-}

{-
	*) Define the function
	• flip :: (a -> b -> c) -> (b -> a -> c)
	which reverses the order in which its function argument takes its arguments.
	The following example shows the effect of flip:
	Prelude> flip div 3 100
	33
-}

flip' :: (a -> b -> c) -> (b -> a -> c)
flip' f = \x y -> f y x
