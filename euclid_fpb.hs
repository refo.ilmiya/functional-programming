fpb x 0 = x
fpb x y = fpb y (x `mod` y)

-- >>> fpb 3 2
-- 1
--
