kpk x y = [z | z <- (map (*x) [1..]) , z `mod` y == 0] !! 0

-- attempt to use euclid's gcd
fpb x 0 = x
fpb x y = fpb y (x `mod` y)

-- mix them up, quot == div
-- (x `quot` y)*y + (x `rem` y) == x  
-- (x `div`  y)*y + (x `mod` y) == x
-- quot is integer division truncated toward zero, while 
-- the result of div is truncated toward negative infinity.
-- The div function is often the more natural one to use, whereas 
-- the quot function corresponds to the machine instruction on modern machines, 
-- so it's somewhat more efficient.

kpkbyfpb x y =  abs ((x `quot` (fpb x y)) * y)
