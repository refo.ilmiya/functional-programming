add [] []         = []
add (a:as) (b:bs) = (a+b) : (add as bs)

fibs  =  1 : 1 : add fibs (tail fibs)

-- >>> fibs !!  23
-- 46368
--
--