-- Number 2
divisor n = [x | x <- [1..n], n `mod` x == 0]