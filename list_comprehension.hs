import Data.List 
-- Number 2
divisor n = [x | x <- [1..n], n `mod` x == 0]

-- Number 3
quickSort [] = []
quickSort (x:xs) = quickSort([y | y <- xs, y <= x]) ++ [x] ++ quickSort([y | y <-xs, y > x])

-- Number 4
perm []  = [[]]
perm ls = [x:ps | x <- ls,ps <- perm(ls\\[x])]

splits []         = [([],[])]
splits (x:xs)     = ([],x:xs) : [(x:ps,qs) | (ps,qs)<-splits xs]

permSplit []         = [[]]
permSplit (x:xs)     = [ ps ++ [x] ++ qs | rs <- permSplit xs, (ps,qs)<- splits rs]


-- Number 5
primes = sieve[2..]
   where sieve (x:xs) = x : (sieve[z | z <- xs, z `mod` x /=0])

-- Number 6
pythaTriple = [(x, y, z) | z <- [5 ..], y <- [4 .. z], x <- [3 .. z], x*x + y*y == z*z]