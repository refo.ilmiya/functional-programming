-- * 3
-- Generating Lists

listOfLength :: Int -> Gen a -> Gen [a]
listOfLength n gen = sequence [ gen | i <- [1..n] ]

pairsOfEqualLengthLists :: Gen a -> Gen ([a],[a])
pairsOfEqualLengthLists gen =
  do n <- choose (0,100)
     xs <- listOfLength (abs n) gen
     ys <- listOfLength (abs n) gen
     return (xs,ys)

prop_ZipUnzip :: [(Int,Int)] -> Bool
prop_ZipUnzip xys =
  zip xs ys == xys
 where
  (xs,ys) = unzip xys

-- simple, but bad, solution

prop_UnzipZip :: [Int] -> [Int] -> Property
prop_UnzipZip xs ys =
  length xs == length ys ==>
    unzip (zip xs ys) == (xs,ys)

-- alternative solution 1

data TwoSameLengthLists a = SameLength [a] [a]
 deriving (Show)

instance Arbitrary a => Arbitrary (TwoSameLengthLists a) where
  arbitrary =
    do (xs,ys) <- pairsOfEqualLengthLists arbitrary
       return (SameLength xs ys)

prop_UnzipZip1 :: TwoSameLengthLists Int -> Bool
prop_UnzipZip1 (SameLength xs ys) =
  unzip (zip xs ys) == (xs,ys)

-- alternative solution 2

prop_UnzipZip2 :: Property
prop_UnzipZip2 =
  forAll (pairsOfEqualLengthLists arbitrary) $ \(xs,ys) ->
    unzip (zip xs ys) == (xs :: [Int],ys :: [Int])